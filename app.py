from count_the_days import *

dt1 = Date(13, 11, 2020)
dt2 = Date(24, 12, 2020)

print(get_difference_between_dates(dt1, dt2), "days")

dt1 = Date(23, 11, 2020)
dt2 = Date(23, 11, 2021)

print(get_difference_between_dates(dt1, dt2), "days")

dt1 = Date(11, 13, 2020)
dt2 = Date(24, 12, 2020)

print(get_difference_between_dates(dt1, dt2), "days")